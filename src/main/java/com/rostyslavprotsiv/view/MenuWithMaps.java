package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MenuWithMaps {

    private Controller controller = Controller.getInstance();
    private Logger Logger = LogManager.getLogger(MenuWithMaps.class);
    private Map<Integer, String> menu;
    private Map<Integer, Printable> methodsMenu;
    private Scanner input;

    public MenuWithMaps() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put(0, "0 : Free mode");
        menu.put(1, "1 : Tutorial");
        menu.put(2, "2 : Training");
        menu.put(3, "3 : Settings");
        menu.put(4, "4 : Authors");
        menu.put(5, "5 : Gameplay");
        menu.put(6, "6 : Exit");
        methodsMenu.put(0, this::freeMode);
        methodsMenu.put(1, this::tutorial);
        methodsMenu.put(2, this::training);
        methodsMenu.put(3, this::settings);
        methodsMenu.put(4, this::authors);
        methodsMenu.put(5, this::gameplay);
        methodsMenu.put(6, this::quit);
    }


    public void show() {
        Integer menuPoint = 0;
        do {
            outMenu();
            Logger.info("Please, select menu point.");
            input = new Scanner(System.in);
            if (input.hasNextInt()) {
                menuPoint = input.nextInt();
                Printable pointer = methodsMenu.get(menuPoint);
                if (pointer == null) {
                    Logger.warn("You entered bad value");
                } else {
                    pointer.print();
                }
            } else {
                Logger.warn("You entered not a number");
            }
        } while (!menuPoint.equals(6));
    }

    private void freeMode() {
        Logger.info("You entered in free mode!");
    }

    private void tutorial() {
        Logger.info("You entered in tutorial!");
    }

    private void training() {
        Logger.info("You entered in training!");
    }

    private void settings() {
        Logger.info("Video");
        Logger.info("Music");
        Logger.info("Control");
    }

    private void authors() {
        Logger.info("Rostyslav");
        Logger.info("Olegovych");
        Logger.info("Protsiv");
    }

    private void gameplay() {
        Logger.info("-> - Turn right");
        Logger.info("<- - Turn left");
        Logger.info("p - Fire");
    }

    private void outMenu() {
        Logger.info("MenuWithMaps:");
        for (String str : menu.values()) {
            Logger.info(str);
        }
    }

    private void quit() {
        Logger.info("Bye, Bye!!");
    }
}