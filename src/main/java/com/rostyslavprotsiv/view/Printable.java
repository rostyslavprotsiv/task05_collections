package com.rostyslavprotsiv.view;

@FunctionalInterface
public interface Printable {

    void print();
}
