package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public final class Menu {
    private static final Menu INSTANCE = new Menu();
    private static final Controller CONTROLLER = Controller.getInstance();
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    private Menu() {
    } // private constructor

    public static Menu getInstance() {
        return INSTANCE;
    }

    public void show() {
        String str = CONTROLLER.getSimpleString();
        if (str == "") {
            String logMessage = "String is empty";
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
        }
        LOGGER.info("Simple string : " + str);
        LOGGER.info("Sorted string using comparable : " + CONTROLLER.getStringWithComparable());
        LOGGER.info("Sorted string using comparator : " + CONTROLLER.getStringWithComparator());
        LOGGER.info("Index (got by using binary search): " + CONTROLLER.getStringUsingBinarySearch());
    }
}
