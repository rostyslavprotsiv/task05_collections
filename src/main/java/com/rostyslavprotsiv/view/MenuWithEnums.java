package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MenuWithEnums {

    private static Logger Logger = LogManager.getLogger(MenuWithEnums.class);
    private Controller controller = Controller.getInstance();
    private MenuPoints menuPoints;
    private MenuMethods menuMethods;
    private Scanner input;

    private enum MenuMethods {
        F(MenuMethods::freeMode), T(MenuMethods::tutorial),
        R(MenuMethods::training), S(MenuMethods::settings),
        A(MenuMethods::authors), G(MenuMethods::gameplay),
        Q(MenuMethods::quit);

        private static Logger Logger = LogManager.getLogger(MenuWithEnums.class);
        private final Printable point;

        MenuMethods(Printable point) {
            this.point = point;
        }

        private static void freeMode() {
            Logger.info("You entered in free mode!");
        }

        private static void tutorial() {
            Logger.info("You entered in tutorial!");
        }

        private static void training() {
            Logger.info("You entered in training!");
        }

        private static void settings() {
            Logger.info("Video");
            Logger.info("Music");
            Logger.info("Control");
        }

        private static void authors() {
            Logger.info("Rostyslav");
            Logger.info("Olegovych");
            Logger.info("Protsiv");
        }

        private static void gameplay() {
            Logger.info("-> - Turn right");
            Logger.info("<- - Turn left");
            Logger.info("p - Fire");
        }

        private static void quit() {
            Logger.info("Bye, Bye!!");
        }


    }

    private enum MenuPoints {
        F("f : FreeMode"), T("t : Tutorial"),
        R("r : Training"), S("s : Settings"),
        A("a : authors"), G("g : gameplay"),
        Q("q : exit");

        private String point;

        MenuPoints(String point) {
            this.point = point;
        }

    }

    public MenuWithEnums() {
        input = new Scanner(System.in);
    }

    private void outMenu() {
        Logger.info("MenuWithEnums:");
        for (MenuPoints point : menuPoints.values()) {
            Logger.info(point.point);
        }
    }

    public void show() {
        String menuPoint;
        do {
            outMenu();
            Logger.info("Please, select menu point.");
            menuPoint = input.nextLine().toUpperCase();
            try {
                menuMethods.valueOf(menuPoint).point.print();
            } catch (IllegalArgumentException e) {
                Logger.warn("You entered not needed data");
            }
        } while (!menuPoint.equals("Q"));
    }
}

