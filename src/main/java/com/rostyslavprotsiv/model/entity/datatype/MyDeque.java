package com.rostyslavprotsiv.model.entity.datatype;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class MyDeque<E> implements Deque<E> {
    private E[] elements;
    private int tale;
//capacity подвоюється, щоб весь час не збульшувати її

    public MyDeque() {
        elements = (E[]) new Object[1];
    }

    public MyDeque(int newCapacity) {
        if (newCapacity > 0) {
            elements = (E[]) new Object[newCapacity];
            tale = newCapacity - 1;
        } else {
            throw new IllegalArgumentException("Bad capacity in constructor");
        }
    }


    @Override
    public void forEach(Consumer<? super E> consumer) {
    }

    @Override
    public boolean isEmpty() {
        return tale == 0;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean removeIf(Predicate<? super E> predicate) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public void clear() {
        elements = (E[]) new Object[1];
    }

    @Override
    public Spliterator<E> spliterator() {
        return null;
    }

    @Override
    public Stream<E> stream() {
        return null;
    }

    @Override
    public Stream<E> parallelStream() {
        return null;
    }

    @Override
    public void addFirst(E e) {
        if (e == null) {
            throw new NullPointerException();
        }
        if (tale == elements.length - 1) {
            expandCapacity();
        }
        for (int i = elements.length - 1; i > 0; i--) {
            elements[i] = elements[i - 1];
        }
        elements[0] = e;
    }


    @Override
    public void addLast(E e) {
        if (e == null) {
            throw new NullPointerException();
        }
        if (tale == elements.length - 1) {
            expandCapacity();
        }
        elements[elements.length - 1] = e;
        tale++;
    }

    @Override
    public boolean offerFirst(E e) {
        return false;
    }

    @Override
    public boolean offerLast(E e) {
        return false;
    }

    @Override
    public E removeFirst() {
        return null;
    }

    @Override
    public E removeLast() {
        return null;
    }

    @Override
    public E pollFirst() {
        return null;
    }

    @Override
    public E pollLast() {
        return null;
    }

    @Override
    public E getFirst() {
        return elements[0];
    }

    @Override
    public E getLast() {
        return elements[tale];
    }

    @Override
    public E peekFirst() {
        return null;
    }

    @Override
    public E peekLast() {
        return null;
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean add(E e) {
        return false;
    }

    @Override
    public boolean offer(E e) {
        return false;
    }

    @Override
    public E remove() {
        return null;
    }

    @Override
    public E poll() {
        return null;
    }

    @Override
    public E element() {
        return null;
    }

    @Override
    public E peek() {
        return null;
    }

    @Override
    public void push(E e) {
    }

    @Override
    public E pop() {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Iterator<E> descendingIterator() {
        return null;
    }

    private void expandCapacity() {
        E[] ensured = (E[]) new Object[elements.length * 2];
        System.arraycopy(elements, 0, ensured, 0, elements.length);
        elements = ensured;
    }
}
