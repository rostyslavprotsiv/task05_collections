package com.rostyslavprotsiv.model.entity.datatype;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class MyBinaryTree<K, V> implements Map<K, V> {

    private Node<K, V> root = null;
    private final Comparator<? super K> comparator;
    private int size = 0;

    public MyBinaryTree() {
        comparator = null;
    }

    public MyBinaryTree(Comparator<? super K> comparator) {
        this.comparator = comparator;
    }

    public Comparator<? super K> comparator() {
        return comparator;
    }

    @Override
    public V getOrDefault(Object o, V v) {
        return null;
    }

    @Override
    public void forEach(BiConsumer<? super K, ? super V> biConsumer) {
    }

    @Override
    public void replaceAll(BiFunction<? super K, ? super V, ? extends V> biFunction) {
    }

    @Override
    public V putIfAbsent(K k, V v) {
        return null;
    }

    @Override
    public boolean remove(Object o, Object o1) {
        return false;
    }

    @Override
    public boolean replace(K k, V v, V v1) {
        return false;
    }

    @Override
    public V replace(K k, V v) {
        return null;
    }

    @Override
    public V computeIfAbsent(K k, Function<? super K, ? extends V> function) {
        return null;
    }

    @Override
    public V computeIfPresent(K k, BiFunction<? super K, ? super V, ? extends V> biFunction) {
        return null;
    }

    @Override
    public V compute(K k, BiFunction<? super K, ? super V, ? extends V> biFunction) {
        return null;
    }

    @Override
    public V merge(K k, V v, BiFunction<? super V, ? super V, ? extends V> biFunction) {
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object o) {
        return false;
    }

    @Override
    public boolean containsValue(Object o) {
        return false;
    }

    @Override
    public V get(Object key) {
        Node<K, V> current = getEntry(key);
        return (current == null ? null : current.value);
    }

    @Override
    public V put(K k, V v) {
        Node<K, V> temporaryNode = root;
        if (temporaryNode == null) {
            root = new Node(k, v, null);
            size++;
            return null;
        } else {
            int cmp;
            Node<K, V> parent;
            do {
                parent = temporaryNode;
                cmp = compare(k, temporaryNode.key);
                if (cmp < 0) {
                    temporaryNode = temporaryNode.left;
                } else if (cmp > 0) {
                    temporaryNode = temporaryNode.right;
                } else if (cmp == 0) {
                    temporaryNode.value = v;
                    return v;
                }
            } while (temporaryNode != null);
            Node<K, V> current = new Node(k, v, parent);
            size++;
            if (cmp > 0) {
                parent.right = current;
            } else if (cmp < 0) {
                parent.left = current;
            }
        }
        return v;
    }

    @Override
    public V remove(Object key) {
        Node<K, V> current = getEntry(key);
        V value = null;
        if (current != null) {
            value = current.value;
            if (current.left != null && current.right != null) {
                allNodesPresent(current);
            } else if (current.left == null && current.right == null) {
                allNodesAbsent(current);
            } else {
                leftOrRightAbsent(current);
            }
            size--;
        }
        return value;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> map) {
    }

    @Override
    public void clear() {
    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    final int compare(Object k1, Object k2) {
        return comparator == null ? ((Comparable<? super K>) k1).compareTo((K) k2)
                : comparator.compare((K) k1, (K) k2);
    }

    private Node<K, V> getEntry(Object key) {
        if (key == null) {
            throw new NullPointerException();
        }
        K findingKey = (K) key;
        Node<K, V> temporaryNode = root;
        int cmp;
        do {
            cmp = compare(findingKey, temporaryNode.key);
            if (cmp < 0) {
                temporaryNode = temporaryNode.left;
            } else if (cmp > 0) {
                temporaryNode = temporaryNode.right;
            } else if (cmp == 0) {
                return temporaryNode;
            }
        } while (temporaryNode != null);
        //if object was not found : return null
        return null;
    }

    private void leftOrRightAbsent(Node<K, V> current) {
        if (current.left != null && current.right == null) {
            current.left.parent = null;
            current.key = current.left.key;
            current.value = current.left.value;
            current.left = current.left.left;
            current.right = current.left.right;
        }
        if (current.left == null && current.right != null) {
            current.right.parent = null;
            current.key = current.right.key;
            current.value = current.right.value;
            current.left = current.right.left;
            current.right = current.right.right;
        }
    }

    private void allNodesAbsent(Node<K, V> current) {
        if (compare(current.key, current.parent.key) < 0) {
            current.parent.left = null;
        } else {
            current.parent.right = null;
        }
    }

    private void allNodesPresent(Node<K, V> current) {
        if (current.left.right != null && current.right.left != null) {
            Node<K, V> hungNode = current.left.right;
            Node<K, V> leftNode = current.right.left;
            current.key = current.left.key;
            current.value = current.left.value;
            current.left.parent = null;
            current.left = current.left.left;
            while (leftNode.left != null) {
                leftNode = leftNode.left;
            }
            hungNode.parent = leftNode.left.parent;
            leftNode.left = hungNode;
        } else if (current.left.right == null) {
            current.left.parent = null;
            current.key = current.left.key;
            current.value = current.left.value;
            current.left = current.left.left;
        } else if (current.right.left == null) {
            current.left.parent = null;
            current.left.right.parent = current.right.left.parent;
            current.right.left = current.left.right;
            current.key = current.left.key;
            current.value = current.left.value;
            current.left = current.left.left;
        }
    }

    static final class Node<K, V> {
        K key;
        V value;
        Node<K, V> left = null;
        Node<K, V> right = null;
        Node<K, V> parent;

        Node(K key, V value, Node<K, V> parent) {
            this.key = key;
            this.value = value;
            this.parent = parent;
        }


        @Override
        public String toString() {
            return "Node{" +
                    "key=" + key +
                    ", value=" + value +
                    ", left=" + left +
                    ", right=" + right +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "MyBinaryTree{" +
                "root=" + root +
                ", comparator=" + comparator +
                ", size=" + size +
                '}';
    }
}
