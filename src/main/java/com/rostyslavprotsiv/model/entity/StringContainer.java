package com.rostyslavprotsiv.model.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StringContainer {
    private static final Logger LOGGER = LogManager.getLogger(StringContainer.class);
    private static final int DEFAULT_CAPACITY = 10;
    private String[] elementData = new String[DEFAULT_CAPACITY];

    /**
     * The size of the ArrayList (the number of elements it contains).
     *
     * @serial
     */
    private int size;


    public StringContainer(int initialCapacity) {
        if (initialCapacity <= DEFAULT_CAPACITY)
            throw new IllegalArgumentException("Illegal Capacity: " +
                    initialCapacity);
        this.elementData = new String[initialCapacity];
    }

    public StringContainer() {
    }

    public int size() {
        return size;
    }

    public void add(String element) {
        if (size == elementData.length) {
            expandCapacity();
        }
        elementData[size++] = element;
    }

    public String get(int index) {
        rangeCheck(index);
        return elementData[index];
    }

    private void rangeCheck(int index) {
        if (index >= size) {
            String logMessage = "Used incorrect index";
            LOGGER.trace(logMessage);
            LOGGER.debug(logMessage);
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
            throw new IndexOutOfBoundsException("Index is too big");
        }
    }

    private void expandCapacity() {
        String[] ensured = new String[elementData.length * 2];
        System.arraycopy(elementData, 0, ensured, 0, elementData.length);
        elementData = ensured;
    }
}
