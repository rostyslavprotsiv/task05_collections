package com.rostyslavprotsiv.model.entity;

import java.util.Comparator;

public class StringComparableComparator implements Comparator<StringComparable> {
    @Override
    public int compare(StringComparable comp1, StringComparable comp2) {
        if (comp1.getSecond().equals(comp2.getSecond())) {
            return 0;
        }
        if (comp1.getSecond().length() == comp2.getSecond().length()) {
            return 0;
        } else if (comp1.getSecond().length() > comp2.getSecond().length()) {
            return -1;
        } else {
            return 1;
        }
    }
}
