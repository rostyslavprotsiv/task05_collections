package com.rostyslavprotsiv.model.entity;

import java.util.Objects;

public class StringComparable implements Comparable<StringComparable> {
    private String first;
    private String second;

    public StringComparable() {
    }

    public StringComparable(String first, String second) {
        this.first = first;
        this.second = second;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    @Override
    public int compareTo(StringComparable comp) {
        if (comp.first.equals(first)) {
            return 0;
        }
        if (comp.first.length() == first.length()) {
            return 0;
        } else if (comp.first.length() > first.length()) {
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringComparable that = (StringComparable) o;
        return Objects.equals(first, that.first) &&
                Objects.equals(second, that.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }

    @Override
    public String toString() {
        return "StringComparable{" +
                "first='" + first + '\'' +
                ", second='" + second + '\'' +
                '}';
    }
}
