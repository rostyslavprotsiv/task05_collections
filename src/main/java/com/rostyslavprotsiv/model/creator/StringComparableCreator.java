package com.rostyslavprotsiv.model.creator;

import com.rostyslavprotsiv.model.entity.StringComparable;

import java.util.ArrayList;

public class StringComparableCreator {

    public ArrayList<StringComparable> create() {
        ArrayList<StringComparable> created = new ArrayList<>();
        created.add(new StringComparable("Dog","Animal"));
        created.add(new StringComparable("Bee","Bug"));
        created.add(new StringComparable("Table","Furniture"));
        created.add(new StringComparable("Bow","Instrument"));
        return created;
    }
}
