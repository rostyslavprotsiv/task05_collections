package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.creator.StringComparableCreator;
import com.rostyslavprotsiv.model.entity.StringComparable;
import com.rostyslavprotsiv.model.entity.StringComparableComparator;

import java.util.ArrayList;
import java.util.Collections;

public class StringComparableSorter {
    public static final StringComparableCreator STRING_COMPARABLE_CREATOR =
            new StringComparableCreator();

    public ArrayList<StringComparable> getSortedWithComparable() {
        ArrayList<StringComparable> sorted = STRING_COMPARABLE_CREATOR.create();
        Collections.sort(sorted);
        return sorted;
    }

    public ArrayList<StringComparable> getSortedWithComparator() {
        ArrayList<StringComparable> sorted = STRING_COMPARABLE_CREATOR.create();
        Collections.sort(sorted, new StringComparableComparator());
        return sorted;
    }
}
