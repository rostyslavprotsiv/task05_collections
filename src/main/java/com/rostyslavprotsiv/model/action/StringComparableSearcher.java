package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.StringComparable;
import com.rostyslavprotsiv.model.entity.StringComparableComparator;

import java.util.ArrayList;
import java.util.Collections;

public class StringComparableSearcher {

    public int binarySearchUsingComparator(ArrayList<StringComparable> allList) {
        StringComparableComparator comp = new StringComparableComparator();
        StringComparable cat = new StringComparable("Dog","Animal");
        return Collections.binarySearch(allList, cat, comp);
    }
}
