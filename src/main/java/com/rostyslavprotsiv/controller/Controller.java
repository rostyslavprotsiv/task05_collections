package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.StringComparableSearcher;
import com.rostyslavprotsiv.model.action.StringComparableSorter;
import com.rostyslavprotsiv.model.creator.StringComparableCreator;
import com.rostyslavprotsiv.model.entity.StringComparable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;

public final class Controller {
    private static final Controller INSTANCE = new Controller();
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private static final StringComparableSorter STRING_COMPARABLE_SORTER
            = new StringComparableSorter();
    private static final StringComparableSearcher STRING_COMPARABLE_SEARCHER
            = new StringComparableSearcher();
    private static final StringComparableCreator STRING_COMPARABLE_CREATOR
            = new StringComparableCreator();

    private Controller() {
    } // private constructor

    public static Controller getInstance() {
        return INSTANCE;
    }


    public String getSimpleString() {
        ArrayList<StringComparable> stringComparables = STRING_COMPARABLE_CREATOR.create();
        return stringComparables.toString();
    }

    public String getStringWithComparable() {
        ArrayList<StringComparable> stringComparables =
                STRING_COMPARABLE_SORTER.getSortedWithComparable();
        return stringComparables.toString();
    }

    public String getStringWithComparator() {
        ArrayList<StringComparable> stringComparables =
                STRING_COMPARABLE_SORTER.getSortedWithComparator();
        return stringComparables.toString();
    }

    public int getStringUsingBinarySearch() {
        ArrayList<StringComparable> stringComparables =
                STRING_COMPARABLE_SORTER.getSortedWithComparator();
        int index = STRING_COMPARABLE_SEARCHER.binarySearchUsingComparator(stringComparables);
        if (index < 0) {
            String logMessage = "Used incorrect arguments : ";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
        }
        return index;

    }

}
