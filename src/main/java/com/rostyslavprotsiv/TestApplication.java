package com.rostyslavprotsiv;

import com.rostyslavprotsiv.model.entity.StringContainer;
import com.rostyslavprotsiv.model.entity.datatype.MyDeque;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class TestApplication {
    private static final Logger LOGGER = LogManager.getLogger(TestApplication.class);

    public static void main(String[] args) {
        testContainer();
        testDeque();
    }

    private static void testContainer() {
        int k = 1000;
        long[] result = new long[2];
        result[0] = 0;
        result[1] = 0;
        long[] help;
        for (int i = 0; i < k; i++) {
            help = whoIsBetter();
            result[0] += help[0];
            result[1] += help[1];
        }
        LOGGER.info("StringContainer : " + result[0] / 100 + ", ArrayList : " + result[1] / 100);
    }

    private static long[] whoIsBetter() {
        final int repetitionTime = 999999;
        ArrayList<String> arr = new ArrayList<>();
        StringContainer container = new StringContainer();
        long st, en, allStringContainerTime = 0, allArrayListTime = 0;
        st = System.nanoTime();
        for (int i = 0; i < repetitionTime; i++) {
            container.add("AB");
            container.add("ABwefew");
            container.add("AgrtgB");
            for (int j = 0; j < 3; j++) {
                container.get(j);
            }
        }
        en = System.nanoTime();
        allStringContainerTime += (en - st);
        allStringContainerTime /= repetitionTime;
        st = System.nanoTime();
        for (int i = 0; i < repetitionTime; i++) {
            arr.add("AB");
            arr.add("ABwefew");
            arr.add("AgrtgB");
            for (int j = 0; j < 3; j++) {
                arr.get(j);
            }
        }
        en = System.nanoTime();
        allArrayListTime += (en - st);
        allArrayListTime /= repetitionTime;
        return new long[]{allStringContainerTime, allArrayListTime};
    }

    private static void testDeque() {
        MyDeque<String> deq = new MyDeque<>();
        deq.addFirst("ad");
        deq.addFirst("ad1");
        deq.addFirst("ad2");
        deq.addFirst("ad3");
        deq.addLast("ad4");
        deq.addLast("ad5");
        deq.addLast("ad6");
        LOGGER.info(deq.getFirst());
    }
}
