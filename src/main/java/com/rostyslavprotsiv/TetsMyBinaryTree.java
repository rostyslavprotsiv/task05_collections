package com.rostyslavprotsiv;

import com.rostyslavprotsiv.model.entity.datatype.MyBinaryTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TetsMyBinaryTree {
    private static final Logger LOGGER = LogManager.getLogger(TetsMyBinaryTree.class);

    public static void main(String[] args) {
        MyBinaryTree<Integer, String> btree = new MyBinaryTree<>();
        btree.put(40, "40s");
        btree.put(80, "80s");
        btree.put(50, "50s");
        btree.put(100, "100s");
        btree.put(45, "45s");
        btree.put(60, "60s");
        btree.put(90, "90s");
        btree.put(110, "110s");
        btree.put(43, "43s");
        btree.put(48, "48s");
        btree.put(42, "42s");
        btree.put(44, "44s");
        LOGGER.info(btree);
        LOGGER.info(btree.get(80));
        LOGGER.info(btree.remove(45));
        LOGGER.info(btree);
        LOGGER.info(btree.get(100));
    }
}
